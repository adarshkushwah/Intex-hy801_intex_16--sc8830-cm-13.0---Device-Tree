# Intex Cloud M6 16GB [Codename : hy801_intex_16]

![intex_cloud_m6_display](https://cloud.githubusercontent.com/assets/28574847/26077084/82145802-39d8-11e7-9cc5-81074a18505e.jpg)

# Display
Type	IPS-LCD
Resolution	854 x 480 pixels
Pixel Density	196 ppi
Screen Size	5 inches

# Memory
RAM	2 GB
Internal Memory	16 GB
Expandable	32 GB (Micro SD card)

# Camera
Rear Camera	8 MP (Face Detection, Smile detection, HDR, Gesture Capture, Quick Shot, Live Photo Mode)
Front Camera	5 MP
Image Stablizer	No
Flash	Yes (LED)
Video Recording	Yes

# Battery
Capacity	2000 mAh
Type	Removable
Talktime	6 hrs
Standby Time	250 hrs

# Dimension
Design	Bar
Size	142 x 71.5 x 8.95 mm
Weight	140 grams

# Operating System And Processor
Processor	Quad Core 1.2 GHz (Spreadtrum SC7731)
Operating System	Android 4.4.2 (Kitkat)

# Connectivity
Network	2G, 3G (GSM 900, GSM 1800 HSDPA 2100)
GSM/CDMA	GSM
Bluetooth	Yes
WiFi	Yes (Wi-Fi 802.11 b/g/n)
Internet	GPRS, EDGE
GPS	With A-GPS
USB	Micro
HDMI	No
Headphone Connector	No
SIM	Dual
NFC	No
DLNA	No

# Multimedia
Radio	Yes
Media Player	Yes (WAV, AAC, AMR, MP3, MIDI, OGG, AAC+, eAAC+)
Video Player	Yes ( 3GP, MP4, AVI, H.264, H.263)

# Additional Feature
Sensors	Proximity, Accelerometer, G- sensor
Built in Applications	Yes (Calculator, Calendar, Phonebook, Notes, To-do list, Organizer, Clock)
Warrenty	1 Year
Keypad	Touchscreen (Capacitive with multitouch)
